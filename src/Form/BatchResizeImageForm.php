<?php

namespace Drupal\batch_resize_image\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BatchResizeImageForm.
 */
class BatchResizeImageForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'batch_resize_image_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['threshold_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Threshold Size'),
      '#description' => $this->t('The file which size is over this bytes will check to resize.'),
      '#default_value' => '2000000',
      '#min' => 0,
      '#weight' => '0',
    ];
    $form['size_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum width or height'),
      '#description' => $this->t('The file which is check to resize will fit the width or height to this number.'),
      '#default_value' => '2048',
      '#min' => 0,
      '#weight' => '1',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Transform'),
      '#weight' => '2',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch['title'] = $this->t('Processing resize image batch');
    $batch['operations'][] = [
      ['Drupal\batch_resize_image\Form\BatchResizeImageForm', 'batchResizeImageProcessBatch'],
      [$form_state->getValue('threshold_size'), $form_state->getValue('size_limit')],
    ];
    $batch['init_message'] = $this->t('Commencing');
    $batch['progress_message'] = $this->t('Processed @current out of @total.');
    $batch['error_message'] = $this->t('An error occurred during processing');
    $batch['finished'] = ['Drupal\batch_resize_image\Form\BatchResizeImageForm', 'batchResizeImageProcessBatchFinished'];

    batch_set($batch);
  }

  /**
   * Batch function to fetch and process all the files information.
   *
   * @param int $size_limit
   *   The size limit filled by user.
   * @param int $threshold_size
   *   The threshold size provided by user.
   * @param array $context
   *   The batch context variable.
   */
  public static function batchResizeImageProcessBatch($size_limit, $threshold_size, array &$context) {
    $db_query = \Drupal::database()->query("SELECT * FROM {file_managed} ");
    $files = $db_query->fetchAll();
    if (empty($context['sandbox'])) {
      // Setup progress.
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($files);
    }

    $db_query_step = \Drupal::database()->queryRange("SELECT * FROM {file_managed}", $context['sandbox']['progress'], 5);
    $files = $db_query_step->fetchAll();
    foreach ($files as $file) {
      $result = [];
      $result['file'] = $file;
      $context['message'] = t('Now processing the files %progress of %max', [
        '%progress' => $context['sandbox']['progress'],
        '%max' => $context['sandbox']['max'] + 1,
      ]
      );
      $context['sandbox']['progress']++;

      if (substr($file->filemime, 0, 5) == 'image') {
        $image = \Drupal::service('image.factory')->get($file->uri);
        $result['image'] = $image;
        $width = $image->getWidth();
        $height = $image->getHeight();
        if ($file->filesize > $threshold_size && ($width > $size_limit || $height > $size_limit)) {
          // Computing the new height and width based on user input.
          if ($width > $height) {
            $width = $size_limit;
            $height = $height * $width / $width;
          }
          else {
            $height = $size_limit;
            $width = $width * $height / $height;
          }

          // Updating the image size as per set threshold.
          $image->resize($width, $height);
          $image->save();

          // Chnaging the image size in the database.
          $file_entity = \Drupal::entityTypeManager()->getStorage('file')->load($file->fid);
          $file_entity->save();
          $result['message'] = '"' . $file->filename . "\" has been transformed.";
          $context['results']['resized'][] = $result;
        }
        else {
          $result['message'] = '"' . $file->source . "\" doesn't need to be transformed.";
          $context['results']['not_resized'][] = $result;
        }
      }
      else {
        $result['message'] = $file->uri . " isn't an image file.";
        $context['results']['not_resized'][] = $result;
      }
      $context['results']['all'][] = $result;
    }
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

  /**
   * The callback for batch finish.
   */
  public static function batchResizeImageProcessBatchFinished($success) {
    if ($success) {
      \Drupal::logger('batch_resize_image')->notice('Batch success');
      \Drupal::messenger()->addMessage(t('Success'));
    }
    else {
      \Drupal::logger('batch_resize_image')->notice('Batch error');
    }
  }

}
