CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides and easy way to resize all the images in the website
in case you forgot to set an upload size limit earlier. It allows user to
provide a size or height/width threshold and based on that images are resized
automatically.


REQUIREMENTS
------------

This module requires only core provided (File and Image) modules to be enabled.


INSTALLATION
------------

 * Install and enable this module like any other drupal 8 module.


CONFIGURATION
-------------

Go to the Configuration -> Media -> Batch Resize Image to access the image
resizing form.


MAINTAINERS
-----------

Current maintainers:
 * jimyhuang (jimyhuang) - https://www.drupal.org/u/jimyhuang
 * Hannes Kirsman (hkirsman) - https://www.drupal.org/u/hkirsman
 * Michael Strelan (mstrelan) - https://www.drupal.org/u/mstrelan
 * Gaurav Kapoor (gaurav.kapoor) - https://www.drupal.org/u/gauravkapoor

This project has been sponsored by:
 * NETivism - https://www.drupal.org/netivism
 * Axelerant - https://www.drupal.org/axelerant
